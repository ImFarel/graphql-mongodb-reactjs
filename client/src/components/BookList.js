import React, { Component } from "react";
import { useQuery } from "@apollo/react-hooks";
import { getBooksQuery } from "../queries/queries";
import BookDetail from "./BookDetail";

function GetBooks({ selectedBook }) {
  const { loading, error, data } = useQuery(getBooksQuery);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p> && console.log(error);

  return data.books.map(({ name, id }) => (
    <li key={id} onClick={e => selectedBook(id)}>{name}</li>
  ));
}

class BookList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedBook: null
    }
    this.eventHandler = this.eventHandler.bind(this)
  }

  eventHandler(id) {
    console.log(id);

    this.setState({
      selectedBook: id
    });
  }

  render() {
    return (
      <div>
        <ul id="book-list">
          <GetBooks selectedBook={this.eventHandler} />
        </ul>
        <div id="book-details">
          {
            this.state.selectedBook ? (<BookDetail bookId={this.state.selectedBook} />) : "No book selected ..."
          }
        </div>
      </div>
    );
  }
}

export default BookList;
