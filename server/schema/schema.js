const graphql = require('graphql');
const Book = require('../models/books');
const Author = require('../models/authors');
const _ = require('lodash');

// dummy data 
// const books = [
//     { name: "Final I", ganre: "Fantasy", id: "1", authorId: "2" },
//     { name: "Covered Man", ganre: "Horor", id: "2", authorId: "3" },
//     { name: "Lunar City", ganre: "Sci-fi", id: "3", authorId: "1" },
//     { name: "Final II", ganre: "Fantasy", id: "1", authorId: "2" },
//     { name: "Covered Man vs Gayung Granny", ganre: "Horor", id: "2", authorId: "3" },
//     { name: "Lunar City Pre-sequel", ganre: "Sci-fi", id: "3", authorId: "1" },
//     { name: "Mission Posible end Mission", ganre: "Action", id: "4", authorId: "2" }
// ];

// const authors = [
//     { name: "Farel", age: 21, id: "1" },
//     { name: "Alel", age: 42, id: "2" },
//     { name: "Rafael", age: 32, id: "3" }
// ];

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLSchema,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull
} = graphql;

const BookType = new GraphQLObjectType({
    name: 'Book',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        ganre: { type: GraphQLString },
        author: {
            type: AuthorType,
            resolve(parent, args) {
                // console.log(parent);
                // return _.find(authors, { id: parent.authorId });
                return Author.findById(parent.authorId);
            }
        }
    })
});

const AuthorType = new GraphQLObjectType({
    name: 'Author',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        age: { type: GraphQLInt },
        books: {
            type: GraphQLList(BookType),
            resolve(parent, args) {
                // console.log(parent);
                // return _.filter(books, { authorId: parent.id });
                return Book.find({ authorId: parent.id });
            }
        }
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        book: {
            type: BookType,
            args: { id: { type: GraphQLID, }, },
            resolve(parent, args) {
                console.info("Request on [book] (Arguments, Parent) => ", args, parent);
                // return _.find(books, { id: args.id });
                return Book.findById(args.id);
            }
        },
        author: {
            type: AuthorType,
            args: { id: { type: GraphQLID, }, },
            resolve(parent, args) {
                console.info("Request on [author] (Arguments, Parent) => ", args, parent);
                // return _.find(authors, { id: args.id });
                return Author.findById(args.id);
            }
        },
        books: {
            type: new GraphQLList(BookType),
            resolve(parent, args) {
                console.info("Request on [books] (Arguments, Parent) => ", args, parent);
                // return books;
                return Book.find();
            }
        },
        authors: {
            type: new GraphQLList(AuthorType),
            resolve(parent, args) {
                console.info("Request on [authors] (Arguments, Parent) => ", args, parent);
                // return authors;
                return Author.find();
            }
        }
    }
});


const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addAuthor: {
            type: AuthorType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString), },
                age: { type: new GraphQLNonNull(GraphQLInt) }
            },
            resolve(parent, args) {
                let author = new Author({
                    name: args.name,
                    age: args.age,
                });
                console.info("Request on [addAuthor] (Arguments, Parent) => ", args, parent);

                return author.save();
            }
        },
        addBook: {
            type: BookType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                ganre: { type: new GraphQLNonNull(GraphQLString) },
                authorId: { type: new GraphQLNonNull(GraphQLID) }
            },
            resolve(parent, args) {
                let book = new Book({
                    name: args.name,
                    ganre: args.ganre,
                    authorId: args.authorId
                });
                console.info("Request on [addBook] (Arguments, Parent) => ", args, parent);
                return book.save();
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
});
