import { gql } from "apollo-boost";

const getBooksQuery = gql`
{
  books {
    id
    name
  }
}
`;

const getBookQuery = gql`
query GetBook($id: ID!){
  book(id: $id){
    id
    name
    ganre
    author {
      id
      name
      books {
        name
        id
      }
    }
  }
}
`;

const getAuthorQuery = gql`
{
  authors {
    id
    name
  }
}
`;



const addBookMutation = gql`
mutation AddBookForm($name: String!, $ganre: String!, $authorId: ID!) {
  addBook(name: $name, ganre: $ganre, authorId: $authorId){
    id
  }
}
`;

export { getAuthorQuery, getBooksQuery, getBookQuery, addBookMutation };