import React, { Component, createRef } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { getAuthorQuery, addBookMutation, getBooksQuery } from "../queries/queries";

function GetAuthor() {
  const { loading, error, data } = useQuery(getAuthorQuery);

  if (loading) return <option>Loading...</option>;
  if (error) return <option>Error :(</option> && console.log(error);

  return data.authors.map(({ id, name }) => (
    <option key={id} value={id}>{name}</option>
  ));
}

function AddBookForm() {
  let input = {};
  let nameInput = createRef();
  let ganreInput = createRef();
  let authorIdInput = createRef();
  const [addBook] = useMutation(addBookMutation, { refetchQueries: [{ query: getBooksQuery }] });

  const changeHandler = (event) => {
    const { name, value } = event.target;
    input[name] = value;
    console.log(input);
  };


  const submitForm = (e) => {
    e.preventDefault();

    addBook({
      variables: input
    });
    input = {};
    nameInput.current.value = ""
    ganreInput.current.value = ""
    authorIdInput.current.value = ""
    console.log(input);

  };

  return (
    <form id="add-book" onSubmit={submitForm}>

      <div className="field">
        <label>Book Name : </label>
        <input id="name" name="name" ref={nameInput} onChange={changeHandler} type="text" />
      </div>

      <div className="field">
        <label>Genre : </label>
        <input id="ganre" name="ganre" ref={ganreInput} onChange={changeHandler} type="text" />
      </div>

      <div className="field">
        <label>Author : </label>
        <select id="authorId" name="authorId" ref={authorIdInput} onChange={changeHandler}>
          <option value="">Select Author</option>
          <GetAuthor />
        </select>
      </div>

      <button>+</button>
    </form >
  );
}

class AddBook extends Component {

  render() {
    return (
      <AddBookForm />
    );
  }
}

export default AddBook;
