const express = require('express');
const graphHttp = require('express-graphql');
const schema = require('./schema/schema');
const MongoDB = require('mongoose');
const cors = require('cors');
// const uri = "mongodb+srv://root:passwierd@cluster0-vxuo1.mongodb.net/graphql_try?retryWrites=true&w=majority";
MongoDB.connect(process.env.MONGO_URI ? process.env.MONGO_URI : `mongodb+srv://root:passwierd@cluster0-vxuo1.mongodb.net/graphql_try?retryWrites=true&w=majority`, { useNewUrlParser: true, useUnifiedTopology: true });
MongoDB.connection.once('open', () => {
    console.log('connected to database✅');
});

const app = express();
// console.

app.use(cors());

app.use('/graphql', graphHttp({
    schema,
    graphiql: process.env.NODE_ENV == "development" ? true : false,
}));

app.listen(4000, () => {
    console.log(`running on ${process.env.NODE_ENV} mode`);
    if (process.env.NODE_ENV == "development") { console.log(`⚒ enabling GraphiQL`); }
    console.log('listening for request on PORT=4000');
    console.log('running on localhost:4000');
});
