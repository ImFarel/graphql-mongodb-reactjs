import React, { Component } from 'react'
import { getBookQuery } from '../queries/queries';
import { useQuery } from '@apollo/react-hooks';
import PropTypes from 'prop-types';

function GetBook({ bookId }) {
  const { loading, error, data } = useQuery(getBookQuery, {
    variables: { id: bookId }
  });

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p> && console.log(error);

  console.log(data);

  return (
    <div>
      <h2><b>{data.book.name}</b></h2>
      <p>{data.book.ganre}</p>
      <p>{data.book.author.name}</p>
      <p>Author Related book :</p>
      <ul className="related-books">
        {data.book.author.books.map(({ id, name }) => (
          <li key={id}>{name === data.book.name ? (<b>{name}</b>) : name}</li>
        ))}
      </ul>
    </div>
  );
}

export default class BookDetail extends Component {
  constructor(props) {
    super(props)
    console.log(props);
  }

  render() {
    return (
      <div>
        <p>Book details</p>
        <GetBook {...this.props} />
      </div>
    )
  }
}

GetBook.propTypes = {
  bookId: PropTypes.string.isRequired,
}